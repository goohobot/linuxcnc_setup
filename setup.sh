#!/bin/bash

set -e

THISDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function fix_usb_serial_permission
{
    echo "Fixing USB/Serial permission problem."
}

function install_packages
{
    sudo apt-get update
    sudo apt-get install -y openssh-server unzip
    sudo apt-get install -y vim git python-pip openjdk-7-jre gddrescue
    # sudo pip install -U pip
    # sudo pip install pyserial
}

function setup_arduino
{
    local ADRUINO_TAR_FILE="arduino-*-linux32.tar.xz"
    cd $THISDIR/arduino

    echo "Checking if there is a single tar file..."
    local COUNT=$(ls -1 $ADRUINO_TAR_FILE | wc -l)
    if [[ $COUNT > 1 ]]; then
        echo "There is more than one file."
        exit 1
    fi

    tar xf $ADRUINO_TAR_FILE
}

function setup_grbl
{
    cd $THISDIR/grbl
    git clone --depth 1 https://github.com/grbl/grbl.git
}

function setup_grbl_tools
{
    cd $THISDIR/grbl/tools

    echo "bCNC"
    sudo apt-get install -y python-tk python-serial python-imaging-tk python-opencv
    git clone --depth 1 https://github.com/vlachoudis/bCNC

    echo "UniversalGcodeSender"
    mkdir UniversalGcodeSender
    unzip UniversalGcodeSender*.zip -d UniversalGcodeSender/
}

function setup_teamviewer
{
    sudo apt-get install -y libjpeg62
    wget -q -O- http://www.teamviewer.com/en/download/linux/ | \
        grep -oP 'http.*teamviewer_i386.deb' | \
        wget -i -
    sudo dpkg -i teamviewer*.deb
}

function setup {
    setup_swap_file
    install_packages
    setup_arduino
    setup_grbl
    setup_grbl_tools
    setup_teamviewer
}

function rsync_to {
    rsync -avz -e "ssh -p 22" ../linuxcnc_setup uusername@192.168.56.101:/home/uusername/Documents/
}

function setup_swap_file
{
    sudo fallocate -l 4G /mnt/swapfile
    sudo chmod 600 /mnt/swapfile
    sudo mkswap /mnt/swapfile
    sudo swapon /mnt/swapfile
    echo '/mnt/swapfile   none    swap    sw    0   0' | sudo tee --append /etc/fstab
}

"$@"
